#!/bin/bash

# set -e

source $OPENSHIFT_CARTRIDGE_SDK_BASH

source ${OPENSHIFT_SBJR_DIR}/bin/sbjr-scripts/util-functions.sh

source ${OPENSHIFT_SBJR_DIR}/bin/sbjr-scripts/setting-java-opt-environment-variables.sh

source ${OPENSHIFT_SBJR_DIR}/bin/sbjr-scripts/setting-VCAP_SERVICES.sh

